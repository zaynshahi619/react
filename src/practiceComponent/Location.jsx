import React from 'react'

const Location = (props) => {
  return (
    <div>
      My country is {props.country}
      <br></br>
      My province is {props.province}
      <br></br>
      My district is {props.district}
      <br></br>
      my exactLocation is {props.exactLocation}
    </div>
  )
}

export default Location