import React from 'react'

const AssignmentTernary = () => {
    let marks = 85
    let age = 17
  return (
    <div>
        {
            marks<=39?<div>fail</div>
            :marks>=40&&marks<60?<div>third division</div>
            :marks>=60&&marks<80?<div>first division</div>
            :marks>=80&&marks<=100?<div>distinction</div>
            :<div>not valid</div>
        }
        {
            age<18?<div>He cannot enter bar</div>
            :<div>He can enter bar</div>
            
        }




    </div>
  )
}

export default AssignmentTernary