import React from 'react'

const LearnTernary = () => {
    let age=19;
    let marks=55
    let calc = () =>{
        if(age<18){
            return(<div>UnderAge</div>)
        }
        else if(age>=18&&age<60){
            return(<div>Adult</div>)
        }
        else{
            return(<div>Old</div>)
        }
    }

  return (
    <div>
        {

            age<18?<div>Underage</div>
            :age>=18&&age<60?<div>Adult</div>
            :<div>Old</div>

        }
        {/* OR */}
        {calc()}


        {
            marks>=40&&marks<60?<div>third division</div>
            :marks>=60&&marks<80?<div>First division</div>
            :<div>Distinction</div>
        }


    </div>
  )
}

export default LearnTernary