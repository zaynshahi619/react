import React from 'react'

import { NavLink } from 'react-router-dom'

const MyLinks = () => {
  return (
    <div className='links'>
        {/* <a href='http://localhost:3000/products/create'>Create product</a> */}
        <NavLink to='http://localhost:3000/products/create' style={{padding:'25px'}}>
            Create product
        </NavLink>
        <NavLink to='http://localhost:3000/products' style={{padding:'25px'}}>
            Product
        </NavLink>
        <NavLink to='http://localhost:3000/students/create' style={{padding:'25px'}}>
            Create Student
        </NavLink>
        <NavLink to='http://localhost:3000/students' style={{padding:'25px'}}>
            Student
        </NavLink>
    </div>
  )
}

export default MyLinks



/*
create product => http://localhost:3000/products/create



*/