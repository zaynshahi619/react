import React from 'react'
import "./productForm.css"

const ProductForm = () => {
  return (
    <div>
      <div className='product-form'>
        <h1>Product Form</h1>
        <form action='' method=''>
            <p>Product Name:</p>
            <input type='text' name="productName" ></input>
            <p>Price</p>
            <input type='number' name='price'></input>
            <p>Quantity</p>
            <input type='number' name='quantity'></input>
            <br></br>
            <button type='submit'>Submit</button>
        </form>
      </div>
    </div>
  )
}

export default ProductForm