import React from 'react'
import { useNavigate, useParams, useSearchParams } from 'react-router-dom'

const ReadSpecificProduct = () => {
    let params = useParams()
    let id = params.id

    let [query] = useSearchParams()
    console.log(query.get("name"));
    console.log(query.get("age"));

    let navigate = useNavigate()
  return (
    <div>
        This is products
        <button onClick={(e)=>{
            navigate("/products/update/213132")
        }}>Update</button>
    </div>
  )
}

export default ReadSpecificProduct