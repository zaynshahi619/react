import React, { useState } from 'react'

const ImageHideShow = () => {
    let [showImage,setShowImage] = useState(true)

    let hideImageFun = (e)=>{
        setShowImage(false)
    }
    let showImageFun = (e)=>{
        setShowImage(true)

    }

  return (
    <div>
        {showImage? <img src='./a1.jpg'></img>:null}
        
        <br></br>
        {/* <button onClick={hideImageFun}>Hide Image</button> */}
        <button onClick={hideImageFun}>Hide Image</button>
        <button onClick={showImageFun}>Show Image</button>

    </div>
  )
}

export default ImageHideShow