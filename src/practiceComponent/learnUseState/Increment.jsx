import React, { useState } from 'react'

const Increment = () => {
    let [count,setCount] = useState(0)
  return (
    <div>
        {count}
        <br></br>
        <button onClick={(e)=>{
            setCount(count+1)
        }}>Increment</button>
    </div>
  )
}

export default Increment