import React, { useState } from 'react'

const ImageHideShow2 = () => {
    let [showImage,setShowImage] = useState(true)

    let handleShow = (isDisplay)=>{
        return (e)=>{
            setShowImage(isDisplay)
        }
    }
  return (
    <div>
         {showImage? <img src='./a1.jpg'></img>:null}
         <br></br>
         <button onClick={handleShow(false)}>Hide Image</button>
         <button onClick={handleShow(true)}>Show Image</button>
    </div>
  )
}

export default ImageHideShow2

//handleClick    (e)=>{}  //used when we do not pass value
//handleClick()   ()=>{return((e)=>{})}   //it is used if we need to pass some value