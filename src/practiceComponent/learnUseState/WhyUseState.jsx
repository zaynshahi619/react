import React, { useState } from 'react'

const WhyUseState = () => {
    let [name,setName] = useState("Hari")
    console.log(name);
    return (
    <div>
        {name}
        <button onClick={()=>{
            setName("ram")
        }}>Click</button>
        {name}

    </div>
  )
}

export default WhyUseState