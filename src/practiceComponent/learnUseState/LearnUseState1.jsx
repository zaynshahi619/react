import React, { useState } from 'react'

const LearnUseState1 = () => {
    // let name = "Manish"
    let [name,setName] = useState("Krishna")
    let [age,setAge] = useState(35)

    let handleClick = (e)=>{
        setName("ram")
    }
    let handleAge = (e)=>{
        setAge(55)
    }
  return (
    <div>
        My name is {name}
        <br></br>
        <button onClick={handleClick}>Click me</button>
        <br></br>
        My age is {age}
        <br></br>
        <button onClick={handleAge}>Change age</button>
    </div>
  )
}

export default LearnUseState1