import React from 'react'

const MapPractice = () => {
    let bestFriend = ["ram","hari","shyam"]
    let BestFriend = ()=>{
        let list = bestFriend.map((item,i)=>{
            return(<div>My best friend is {item}</div>)
        })

        return list
    
    }
  return (
    <div>
        {
            bestFriend.map((item,i)=>{
                return(<div>My best friend is {item}</div>)
            })
        }


        {/* OR */}
        {
            BestFriend()
        }
    </div>
  )
}

export default MapPractice