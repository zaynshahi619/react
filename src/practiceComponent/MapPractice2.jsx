import React from 'react'
import { products } from '../productData'

const MapPractice2 = () => {
    
      let task = ()=>{
        let desireOutput = products.map((item,i)=>{
            return(<div>{item.title}</div>)
        })
        return desireOutput
      }

      let task2 = ()=>{
        let price = products.map((item,i)=>{
            return(<p>{item.title} costs NRS {item.price} and its category is {item.category}</p>)
        })
        return price
      }

      let task3 = ()=>{
        let filterOutput = products.filter((item,i)=>{
            if(item.price>2000){
                return true
            }
           
        })
      
        let desireOutput = filterOutput.map((item,i)=>{
            return(<p>{item.title} costs NRS. {item.price} and its category is {item.category}</p>)
        })
        return desireOutput
        
      }

      let task4 =()=>{
        let filterOutput = products.filter((item,i)=>{
            if(item.category === "Books"){
                return true
            }
        })
        let desireOutput = filterOutput.map((item,i)=>{
            return(<p>The {item.title} costs NRs. {item.price} and its category is {item.category}</p>)
        })
        return desireOutput
      }

      let task5 =()=>{
        let filterPrice = products.map((item,i)=>{
            return(item.price)
        })
        
       

        let totalPrice = filterPrice.reduce((pre,cur)=>{
            return (pre+cur)
        },0)
        return totalPrice
      }

      

  return (
    <div>
        
            {/* <h1>the products are</h1>
            {task()} */}

            {/* {
                task2()

            } */}
            {/* {
               task3()
            } */}
            {/* {
                task4()
            } */}
            {
                <p>The total price of the products are {task5()}</p>
            }
        
    </div>
  )
}

export default MapPractice2