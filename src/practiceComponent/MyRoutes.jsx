import React from 'react'
import { Route, Routes } from 'react-router-dom'
import ProductForm from './Product/ProductForm'
import ReadAllProduct from './Product/ReadAllProduct'
import StudentForm from './Student/StudentForm'
import ReadAllStudents from './Student/ReadAllStudents'
import ReadSpecificProduct from './Product/ReadSpecificProduct'
import ReadSpecificStudent from './Student/ReadSpecificStudent'

const MyRoutes = () => {
  return (
    <div>
        <Routes>
            <Route path='/products/create' element={<ProductForm></ProductForm>}></Route>
            <Route path='/products/:id' element={<ReadSpecificProduct></ReadSpecificProduct>}></Route>

            <Route path='/products' element={<ReadAllProduct></ReadAllProduct>}></Route>
            
            <Route path='/students/create' element={<StudentForm></StudentForm>}></Route>
            <Route path='/students/:id' element={<ReadSpecificStudent></ReadSpecificStudent>}></Route>

            <Route path='/students' element={<ReadAllStudents></ReadAllStudents>}></Route>
        </Routes>
    </div>
  )
}

export default MyRoutes