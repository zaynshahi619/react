import React, { useEffect, useState } from 'react'

const LearnUseEffect2 = () => {
    //we can have multiple useEffect
    let [count1,setCount1] = useState(0)
    let [count2,setCount2] = useState(100)

    useEffect(()=>{
        console.log("i am useEffect function");
    },[count1,count2])

    useEffect(()=>{
        console.log("i am 2 useEffect ");
    },[])

    useEffect(()=>{
        console.log("hello");
    })
//for no dependency useEffect(fun) => fun will execute in each render
// for empty dependency useEffect(fun,[]) fun will execute at first render only
//for [count1,count2] dependency useEffect(fun,[count1,count2])

  return (
    <div>
        {count1}
        <br></br>
        <button onClick={(e)=>{
            setCount1(count1+1)
        }}>Increment</button>
        <br></br>
        {count2}
        <br></br>
        <button onClick={(e)=>{
            setCount2(count2+1)
        }}>Increment</button>
    </div>
  )
}

export default LearnUseEffect2