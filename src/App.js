import "./App.css";
import Detail from "./practiceComponent/Detail";
import Image from "./practiceComponent/Image";
import Info from "./practiceComponent/Info";
import Location from "./practiceComponent/Location";
import Name from "./practiceComponent/Name";
import Age from "./practiceComponent/age";
import LearnTernary from "./practiceComponent/LearnTernary";
import DifferentDataEffect from "./practiceComponent/DifferentDataEffect";

import AssignmentTernary from "./practiceComponent/AssignmentTernary";
import MapPractice from "./practiceComponent/MapPractice";
import MapPractice2 from "./practiceComponent/MapPractice2";
import ButtonClick from "./practiceComponent/ButtonClick";
import LearnUseState1 from "./practiceComponent/learnUseState/LearnUseState1";
import Count from "./practiceComponent/learnUseState/Count";
import ImageHideShow from "./practiceComponent/learnUseState/ImageHideShow";
import ImageHideShow2 from "./practiceComponent/learnUseState/ImageHideShow2";
import Toggle from "./practiceComponent/learnUseState/Toggle";
import WhyUseState from "./practiceComponent/learnUseState/WhyUseState";
import LearnUseEffect1 from "./practiceComponent/LearnUseEffect/LearnUseEffect1";
import LearnUseEffect2 from "./practiceComponent/LearnUseEffect/LearnUseEffect2";
import LearnCleanUpFunction from "./practiceComponent/LearnUseEffect/LearnCleanUpFunction";
import { useState } from "react";
import MyLinks from "./practiceComponent/MyLinks";
import MyRoutes from "./practiceComponent/MyRoutes";

function App() {
  let tag1 = <div>this is div</div>;
  let a = 1;
  let b = 2;

  let [showCom, setShowCom] = useState(true);

  return (
    <div>
      {/* <div style={{color:"red",backgroundColor:"black"}}>My name is Manish</div>
      <div className='success'>this is class dw9</div>
      <div className='failure'>this is class dw8</div>
      {tag1}
      {a+b} */}

      {/* <Name></Name>
      <Age></Age> */}
      {/* <Location
        country="Nepal"
        province="Bagmati"
        district="Kathmandu"
        exactLocation="sankhamul"
      ></Location> */}
      {/* <Detail name="ram" age='25' address="kathmandu"></Detail> */}

      {/* <Info name="Ram" age={25} fatherDetail = {{fname:"kishor",fage:65}} favFood={[" mutton"," chicken"," spinach"]}></Info>
      <Image></Image> */}

      {/* <LearnTernary></LearnTernary> */}
      {/* <DifferentDataEffect></DifferentDataEffect> */}
      {/* <AssignmentTernary></AssignmentTernary> */}
      {/* <MapPractice></MapPractice> */}
      {/* <MapPractice2></MapPractice2> */}
      {/* <ButtonClick></ButtonClick> */}
      {/* <LearnUseState1></LearnUseState1> */}
      {/* <Count></Count> */}
      {/* <ImageHideShow></ImageHideShow> */}
      {/* <ImageHideShow2></ImageHideShow2> */}
      {/* <Toggle></Toggle> */}
      {/* <WhyUseState></WhyUseState> */}
      {/* <LearnUseEffect1></LearnUseEffect1> */}
      {/* <LearnUseEffect2></LearnUseEffect2> */}
      {/* {showCom ? <LearnCleanUpFunction></LearnCleanUpFunction> : null}
      <button
        onClick={(e) => {
          setShowCom(true);
        }}
      >
        show
      </button>
      <button
        onClick={(e) => {
          setShowCom(false);
        }}
      >
        hide
      </button> */}
      <MyLinks></MyLinks>
      <MyRoutes></MyRoutes>
    </div>
  );
}

export default App;
