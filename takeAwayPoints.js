/*

day 1

1)We must return only one Wrapper
2)Always add images in public
3)In img tag ./ means public folder
4)block element
    -it always start from new line
    -it takes all space
5)inline elements
    -it only takes space as it requires
6)We cannot give style of br tag
7)In react we can store html element inside variable
8)Anything that is written inside return are only displayed in browser
9)We can call javascript operation inside tag using {}



day 2

Component
it is a function
whose first letter must be capital 
generally it returns something (generally it return html tag)
component is a custom tag
custom tag does not support any props like style, className, href

Difference between function and component
-Component is a function whose first letter is capital and it return something we call function using parenthesis but we call component using html tag


what is Props?
->passing value in component is called props
->if props is other than string then use {}


day 3
->We cannot use if, else, for, while, do-while inside tag but we can use array loop
->we cannot define variable inside curly braces
{} return only one thing

Ternary operator
we must have else part

Boolean are not shown in the browser

array
->wrapper are disappear
->element are placed one by one without comma


we cannot use object as react child


day 6
->When normal variable is changed page will not render
 but when useState variable is changed page will render

->if any state variable is change then
the component will re execute
such that the state variable which is updated hold the updated value
and other state variable hold previous value
note it is not necessary , a component will render when setName, setAge or set... is called 
other things work normally

->when page gets render first all content of components gets removed

->useEffect function is a asynchronous function
  it is always execute at last (after printing the content to the browser)


day 7
->At first render useEffect function execute 
  but from second render useEffect function depends on its dependency
  if one of the dependency changes useEffect function will execute


day 8
npm i react-router-dom
while selecting component it will see the more specific routes






















*/