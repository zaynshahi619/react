/*
1)Why first letter of component is capital?
    ->The first letter of component being capital is not compulsory but a convention to follow that helps differentiate between regular HTML tags and   React Component


2)Make a component Location , pass props as (country, province, district, exactLocation)
->Done


3)Make a component Info pass a pros as name in string age in number fatherDetail in object {fname:"...", fage:...} 
favFood in array ["mutton", "chicken", "spinacn"]
->Done


4)Difference between function and component?
->-Component is a function whose first letter is capital and it return something.
 we call function using parenthesis but we call component using html tag


5)What is props?
->"props" is short for "properties," and it's a fundamental concept used for passing data from one component to another


6)Make a component with name Images ,This component must display 3 images form public
->Done


7)Explain the cases where  you must used {} Inside children In a props
->for dynamically rendering content or evaluating JavaScript expressions
->Passing Dynamic Data as props


8)Explain why double curly braces is used style props
->when the dynamic data are other than string we use double curly braces



*/